#!/usr/bin/env bash -x

mkdir -p $PREFIX/jar
# Drop the version # in the jar filename so the shell wrapper doesn't have to adjust.
cp stationxml-validator-${PKG_VERSION}.jar $PREFIX/jar/stationxml-validator.jar

pwd
ls

mkdir -p $PREFIX/bin
cp ${RECIPE_DIR}/stationxml-validator $PREFIX/bin/stationxml-validator
